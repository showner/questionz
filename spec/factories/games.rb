# frozen_string_literal: true
FactoryBot.define do
  factory :game do
    user
    question_set
  end
end
