# frozen_string_literal: true
FactoryBot.define do
  factory :question_set do
    title { Faker::Lorem.word }
  end
end
