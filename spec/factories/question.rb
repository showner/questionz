# frozen_string_literal: true
FactoryBot.define do
  factory :question do
    title { Faker::Lorem.sentence(word_count: 2, random_words_to_add: 5) }
    answer  { "true" }
    proposal_0 { rand(5..10).to_s }
    proposal_1 { rand(5..10).to_s }
    proposal_2 { rand(5..10).to_s }
    question_set
  end
end
