# frozen_string_literal: true
require "rails_helper"

RSpec.describe(Game, type: :model) do
  describe "association" do
    it { is_expected.to(belong_to(:question_set).class_name("QuestionSet")) }
    it { is_expected.to(belong_to(:user).class_name("User")) }
  end

  describe "factory" do
    subject(:game) { create(:game) }

    it { is_expected.to(be_valid) }
  end
end
