# frozen_string_literal: true
require "rails_helper"

RSpec.describe(User, type: :model) do
  describe "association" do
    it { is_expected.to(have_many(:games).class_name("Game").dependent(:destroy)) }
  end
end
