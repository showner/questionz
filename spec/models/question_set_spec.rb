# frozen_string_literal: true
require "rails_helper"

RSpec.describe(QuestionSet, type: :model) do
  describe "association" do
    it { is_expected.to(have_many(:questions).class_name("Question").dependent(:destroy)) }
    it { is_expected.to(have_many(:games).class_name("Game").dependent(:destroy)) }
  end

  describe "validations" do
    it { is_expected.to(validate_presence_of(:title)) }
  end

  describe "factory" do
    subject(:question_set) { create(:question_set) }

    it { is_expected.to(be_valid) }
  end
end
