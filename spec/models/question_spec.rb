# frozen_string_literal: true
require "rails_helper"

RSpec.describe(Question, type: :model) do
  describe "association" do
    it { is_expected.to(belong_to(:question_set).class_name("QuestionSet")) }
  end

  describe "validations" do
    it { is_expected.to(validate_presence_of(:title)) }
    it { is_expected.to(validate_presence_of(:answer)) }
    it { is_expected.to(validate_presence_of(:proposal_0)) }
    it { is_expected.to(validate_presence_of(:proposal_1)) }
    it { is_expected.to(validate_presence_of(:proposal_2)) }
  end

  describe "factory" do
    subject(:question) { create(:question) }

    it { is_expected.to(be_valid) }
  end
end
