# frozen_string_literal: true
class Question < ApplicationRecord
  belongs_to :question_set

  validates :title, :answer, :proposal_0, :proposal_1, :proposal_2, presence: true
  validates_associated :question_set

  def picks
    [].push(answer, proposal_0, proposal_1, proposal_2).shuffle
  end
end
