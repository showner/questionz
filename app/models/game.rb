# frozen_string_literal: true
class Game < ApplicationRecord
  belongs_to :user
  belongs_to :question_set

  def correct(params)
    question_set.questions.each_with_index do |question, i|
      self.result += 1 if params["question#{i}".to_sym] == question.answer
    end
    save
  end
end
