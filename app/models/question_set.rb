# frozen_string_literal: true
class QuestionSet < ApplicationRecord
  has_many :questions, dependent: :destroy
  has_many :games, dependent: :destroy

  validates :title, presence: true
  validates_length_of :questions, maximum: 10
end
