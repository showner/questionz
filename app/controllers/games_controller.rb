# frozen_string_literal: true
class GamesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_game, only: [:finish]

  def new_game
    @game = Game.create(question_set: QuestionSet.all.sample, user: current_user)
  end

  def finish
    @game.correct(finish_game_params)
    redirect_to(results_path)
  end

  def results
    @games = current_user.games
  end

  private

  def find_game
    @game = Game.find(params[:id])
  end

  def finish_game_params
    params.require(:game).permit(:question0, :question1, :question2, :question3, :question4,
      :question5, :question6, :question7, :question8, :question9)
  end
end
