# frozen_string_literal: true
class QuestionsController < ApplicationController
  before_action :find_question, only: [:destroy]
  before_action :find_question_set, only: [:create, :destroy]
  before_action :question_set_count, only: [:create]

  def create
    @question = Question.create(question_params.merge(question_set: @question_set))
    redirect_to(edit_question_set_path(@question_set))
  end

  def destroy
    @question.destroy
  end

  private

  def find_question
    @question = Question.find(params[:id])
  end

  def find_question_set
    @question_set = QuestionSet.find(params[:question_set_id])
  end

  def question_params
    params.require(:question).permit(:title, :answer,
      :proposal_0, :proposal_1, :proposal_2)
  end

  def question_set_count
    redirect_to(edit_question_set_path(@question_set),
      alert: "Already 10 questions") if @question_set.questions.count >= 10
  end
end
