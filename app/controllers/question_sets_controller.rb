# frozen_string_literal: true
class QuestionSetsController < ApplicationController
  before_action :find_question_set, only: [:edit, :update]

  def index
    @question_sets = QuestionSet.all
    @question_set = QuestionSet.new
  end

  def create
    @question_set = QuestionSet.create(question_set_params)
    redirect_to(question_sets_path)
  end

  def edit
  end

  def update
    @question_set.update(question_set_params)
    redirect_to(edit_question_set_path(@question_set), notice: "Updated!")
  end

  private

  def find_question_set
    @question_set = QuestionSet.find(params[:id])
  end

  def question_set_params
    params.require(:question_set).permit(:title)
  end
end
