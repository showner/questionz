# frozen_string_literal: true
Rails.application.routes.draw do
  devise_for :users, controllers: {
    confirmations: "users/confirmations",
  }
  root to: "homes#index"

  get "/about", to: "homes#about"

  resources :question_sets, except: [:new, :show, :destroy] do
    resources :questions, only: [:create, :destroy]
  end

  get "/new_game", to: "games#new_game", as: "new_game"
  post "/finish/:id", to: "games#finish", as: "finish"
  get "/results", to: "games#results", as: "results"

  mount LetterOpenerWeb::Engine, at: "/inbox", as: "inbox"
end
