# frozen_string_literal: true
class CreateQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table(:questions) do |t|
      t.text(:title)
      t.text(:answer, null: false)
      t.text(:proposal_0, null: false)
      t.text(:proposal_1, null: false)
      t.text(:proposal_2, null: false)
      t.belongs_to(:question_set, null: false, foreign_key: { to_table: :question_sets })

      t.timestamps
    end
  end
end
