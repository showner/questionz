# frozen_string_literal: true
class CreateGames < ActiveRecord::Migration[6.1]
  def change
    create_table(:games) do |t|
      t.belongs_to(:user, null: false, foreign_key: true)
      t.belongs_to(:question_set, null: false, foreign_key: true)
      t.integer(:result, default: 0)

      t.timestamps
    end
  end
end
